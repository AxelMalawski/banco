import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
				
		Scanner entrada = new Scanner(System.in);
		ArrayList<CuentaCorriente> cuentas = new ArrayList<CuentaCorriente>();
		
			CuentaCorriente cuenta1 = new CuentaCorriente("Virginia Salinas", 5300.00, 1);
			CuentaCorriente cuenta2 = new CuentaCorriente("Axel Malawski", 335300.00, 2);
			CuentaCorriente cuenta3 = new CuentaCorriente("Pedro Lopez", 53020.00, 3);
			CuentaCorriente cuenta4 = new CuentaCorriente("Hugo Gimenez", 5210.00, 4);
			CuentaCorriente cuenta5 = new CuentaCorriente("Luis Moyano", 12300.00, 5);
			
			cuentas.add(cuenta1);
			cuentas.add(cuenta2);
			cuentas.add(cuenta3);
			cuentas.add(cuenta4);
			cuentas.add(cuenta5);
							
	
			try {
			
			switch(menu()) {
			case 1: 
					for(CuentaCorriente cuenta: cuentas ) {
						System.out.println(cuenta.toString());
					}
					realizarOtraOperacion();
					break;
					
			case 2: 
				
				System.out.println("Ingrese el n�mero de cuenta a operar: \n");
				
				for(CuentaCorriente cuenta: cuentas ) {
					System.out.println(cuenta.toString());
				}
			
				int nCuenta;
				double retiro;
				
				nCuenta = entrada.nextInt();
				
				System.out.println("Ingrese el dinero a retirar");
				retiro = entrada.nextDouble();
				
				
				cuentas.get(nCuenta - 1).retirarDinero(retiro);;
				break;
				
			case 3: 
				
				System.out.println("Ingrese el n�mero de cuenta a operar: \n");
				
				for(CuentaCorriente cuenta: cuentas ) {
					System.out.println(cuenta.toString());
				}
						
				nCuenta = entrada.nextInt();
				
				int receptor;
				double dinero;
				

				
				System.out.println("Ingrese el n�mero de cuenta a la que quiere enviar dinero: \n");
			
				receptor = entrada.nextInt();
				
				System.out.println("Ingrese el dinero a enviar \n");
				
				dinero = entrada.nextDouble();
				
				cuentas.get(nCuenta - 1).transferirDinero(cuentas.get(receptor - 1), dinero);;
				
			case 4: 
				
				System.out.println("Ingrese el n�mero de la cuenta a operar: \n");
				
				for(CuentaCorriente cuenta: cuentas ) {
					System.out.println(cuenta.toString());
				}
			
				nCuenta = entrada.nextInt();
				
				cuentas.get(nCuenta - 1).verSaldo();
				break;
				
			case 5: 
			
				System.out.println("Ingrese el n�mero de la cuenta a operar: \n");
				
				for(CuentaCorriente cuenta: cuentas ) {
					System.out.println(cuenta.toString());
				}
				
				nCuenta = entrada.nextInt();
				
				cuentas.get(nCuenta - 1).toString();
				realizarOtraOperacion();
				break;
			
			
			case 6: 
				
				
				System.out.println("Ingrese el n�mero de la cuenta a operar: \n");
				
				for(CuentaCorriente cuenta: cuentas ) {
					System.out.println(cuenta.toString());
				}
				
				nCuenta = entrada.nextInt();
				
				System.out.println("Ingrese el dinero a depositar: \n");
				
				dinero = entrada.nextDouble();
				
				cuentas.get(nCuenta - 1).ingresarDinero(dinero);
							
				realizarOtraOperacion();
				break;
			
			 default: System.out.println("Seleccion� una opcion incorrecta");
					  realizarOtraOperacion();
					  break;
			
			 case 7: 
				 
				 System.out.println("Se guardar�n las cuentas en un archivo...");

				 try { 
				 	FileWriter escritura = new FileWriter("C:/Users/Axel/Desktop/Axel/cuentas.txt");
				 	for (CuentaCorriente cuenta : cuentas) {
				 		for(int i = 0 ; i < cuenta.toString().length() ; i++){
				 			escritura.write(cuenta.toString().charAt(i));
				 		}	
				 	}

				 	System.out.println("Cuentas guardadas");
				 	
				 	escritura.close();

				 } catch (Exception e) {
				 	System.out.println("El archivo no se pudo leer o crear");
				 }
				 
				 break;				  
			}
	
			}catch(Exception e) {
				System.out.println("Seleccion� una opcion incorrecta");
				realizarOtraOperacion();
			}
	}

	public static int menu() {
		
		Scanner entrada = new Scanner(System.in);
		
		int opcion;
		
		System.out.println("----- MEN� CUENTA DE AHORRO ----- \n\n\n1) Listar cuentas.\n2) Retirar dinero.\n3) Transferir dinero.\n4) Ver saldo.\n5) Ver datos.\n6) Ingresar dinero.\n7) Guardar lista de cuentas");
		opcion = entrada.nextInt();
		
		return opcion;
	}
	
	public static void realizarOtraOperacion() {
		
		Scanner entrada = new Scanner(System.in);
		int opcion = 0;
		
		System.out.println("�Quiere realizar otra operaci�n?\n1)Si.\n2)No.");
		opcion = entrada.nextInt();
		
		switch(opcion) {
			
		case 1:	main(null);
			break;
		case 2: break;
				
		}
		
	}
	
	

}
