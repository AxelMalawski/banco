
public class CuentaCorriente {

	
	private String nombreTitular;
	private Double saldoInicial;
	private int nCuentaCorriente;
		
	public CuentaCorriente(String nombreTitular, Double saldoInicial, int nCuentaCorriente) {
		
		this.nombreTitular = nombreTitular;
		this.saldoInicial = saldoInicial;
		this.nCuentaCorriente = nCuentaCorriente;
			
	}


	public void ingresarDinero(Double dinero) {
		this.saldoInicial = this.saldoInicial + dinero;
		Principal.realizarOtraOperacion();
	}
	
	public void retirarDinero(Double retiro) {
		
		if(retiro > this.saldoInicial) {
			System.out.println("Saldo insuficiente\n");
			Principal.main(null);
		}
		
		else this.saldoInicial -= retiro; 
		System.out.println("Ahora el saldo de la cuenta es de: " + this.saldoInicial);
		Principal.realizarOtraOperacion();
	
	}
	
	public void verSaldo() {
		System.out.println("El saldo de la cuenta es de " + this.saldoInicial + "$");
		Principal.realizarOtraOperacion();
	}


	public String toString() {
		return (this.nombreTitular + ", N�: " + this.nCuentaCorriente + " Saldo: " + this.saldoInicial + "$\n");
	}	
	
	public void transferirDinero(CuentaCorriente cuenta, Double dinero) {
		
		if(dinero > this.saldoInicial) {
			System.out.println("Saldo insuficiente\n");
			Principal.main(null);
		}
			
		cuenta.saldoInicial = cuenta.saldoInicial + dinero;
		this.saldoInicial = this.saldoInicial - dinero;
	
		System.out.println("El saldo de su cuenta ahora es: " + this.saldoInicial );
		System.out.println("El saldo de la cuenta destinatario es " + cuenta.saldoInicial );
		Principal.realizarOtraOperacion();
	
	}
}

